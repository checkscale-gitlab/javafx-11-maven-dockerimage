FROM maven:3-jdk-11

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	libxtst6 libgtk2.0-0 \
	; \
	rm -rf /var/lib/apt/lists/*
